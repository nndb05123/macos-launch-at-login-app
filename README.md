# How to launch a macOS app at login?

This is a demo app how to auto-launch a sandboxed macOS application at login - written in Swift 4

### License

[WTFPL](LICENSE)
